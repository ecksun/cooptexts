import kotlin.math.sign

data class Saldo(
    val used: Int,
    val left: Int,
    val transactions: List<Transaction>
)

data class Transaction(
    val date: String,
    val amount: Int,
    val description: String
)

fun parse(input: String): Saldo? {
    val usedRegex = "Utnyttjat belopp: ([0-9]+),([0-9]+)"
    val leftRegex = "Kvar att utnyttja: ([0-9]+),([0-9]+)"

    val pattern = Regex("$usedRegex $leftRegex (.*) Mvh Coop Mastercard")
    val result = pattern.find(input)
    val used = result?.let { getAmount(it, 1)}
    val left = result?.let { getAmount(it, 3)}


    val rawTransactions = result?.groupValues?.getOrNull(5)

    val dateRegex = Regex("[0-9]+ ...")
    val currencyRegex = Regex("(-?[0-9]+),([0-9]+)")

    val words = rawTransactions?.split(" ")
    val transactionStarts = words
        ?.windowed(4, 1)
        ?.foldIndexed(listOf(), fun (index: Int, starts: List<Int>, window: List<String>) : List<Int> {
            if (
                dateRegex.matches(window[0] + " " + window[1]) &&
                currencyRegex.matches(window[2]) &&
                window[3] == "SEK") {
                return starts + listOf(index)
            }
            return starts
        })

    val transactions = transactionStarts?.let { splitTransactions(words, transactionStarts) }
    val bar = transactions?.map { parseTransaction(it) }

    return Saldo(
        used ?: return null,
        left ?: return null,
        bar ?: return null
    )
}

fun getAmount(result: MatchResult, matchGroupStart: Int) : Int? {
    val usedMatch = result.groupValues
    val usedSEK = usedMatch.getOrNull(matchGroupStart)?.toInt()
    val usedCentiSEK = usedMatch.getOrNull(matchGroupStart + 1)?.toInt()
    val used = usedSEK ?: return null
    return used * 100 + used.sign * (usedCentiSEK ?: 0)
}

fun splitTransactions(words: List<String>, starts: List<Int>): List<String> {
    // Get the range of each transaction. We need to include words.size to also include the last transaction
    val lengths = (starts + listOf(words.size))
        .windowed(2, 1)
        .fold(listOf<IntRange>()
        ) { lengths, startend -> lengths + listOf(IntRange(startend[0], startend[1] - 1)) }

    return lengths
        .fold(listOf())
        { transactions, range -> transactions + words.slice(range).joinToString(" ") }
}

fun parseTransaction(input: String) : Transaction{
    val words = input.split(" ")
    val date = words[0] + " " + words[1]
    val currencyRegex = Regex("(-?[0-9]+),([0-9]+)")
    val amount = currencyRegex.find(words[2])?.let { getAmount(it, 1) }
    val description = words.takeLast(words.size - 4).joinToString(" ")
    return Transaction(
        date,
        amount !!, // We assume this works as we have already matched it against the same regex earlier
        description
    )
}