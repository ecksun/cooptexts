package test

import org.junit.jupiter.api.Test
import parse
import Transaction
import java.io.File
import kotlin.test.assertEquals

class ParserTest {
    @Test
    fun testFoo() : Unit {
        val input = File("src/test/resources/basic").readText(Charsets.UTF_8)
        val saldo = parse(input)
        assertEquals(123456, saldo?.used)
        assertEquals(2345678, saldo?.left)
        assertEquals(listOf(
            Transaction(
                "09 jun",
                -1000,
                "EN BUTIK"
            ),
            Transaction(
                "08 jun",
                -2345,
                "EN ANNAN BUTIK"
            )
        ), saldo?.transactions)
    }
}